import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix, accuracy_score
from sklearn.model_selection import train_test_split, KFold
from sklearn.preprocessing import LabelEncoder, MinMaxScaler
from sklearn.tree import DecisionTreeClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import precision_recall_fscore_support
from sklearn.model_selection import cross_val_score

dataset = pd.read_csv("Churn_Modelling.csv", sep=',')

column_selector = ['CreditScore', 'Geography', 'Gender', 'Age', 'Tenure', 'Balance', 'NumOfProducts', 'HasCrCard',
                   'IsActiveMember', 'EstimatedSalary', 'Exited']

dataset = dataset[column_selector]
outliers = ["CreditScore", "Age", "NumOfProducts"]

le = LabelEncoder()
dataset['Gender'] = le.fit_transform(dataset['Gender'])
dataset['Geography'] = le.fit_transform(dataset['Geography'])

X = dataset.iloc[:,:-1]
y = dataset.iloc[:,-1]
X_train, X_test, y_train, y_test = train_test_split(X,y,test_size=0.33)


model_DecisionTree = DecisionTreeClassifier()
model_DecisionTree.fit(X_train, y_train)

model_Bayes = GaussianNB()
model_Bayes.fit(X_train, y_train)

model_LogisticRegression = LogisticRegression()
model_LogisticRegression.fit(X_train, y_train)

def remove_outlier():
    for i in range(len(outliers)):
        first_q = np.percentile(dataset[outliers[i]],25)
        third_q = np.percentile(dataset[outliers[i]],75)
        IQR = third_q - first_q
        IQR *= 1.5
        minimum = first_q - IQR
        maximum = third_q + IQR

        mean = dataset[outliers[i]].median()

        dataset.loc[dataset[outliers[i]] < minimum, outliers[i]] = mean
        dataset.loc[dataset[outliers[i]] > maximum, outliers[i]] = mean





def ScaleData():
    column_normalize_selector = ['CreditScore', 'Age', 'Balance', 'EstimatedSalary']
    scalar = MinMaxScaler()
    dataset[column_normalize_selector] = scalar.fit_transform(dataset[column_normalize_selector])

def TreatCategorical():
    print(" ")

def LogisticRegression():
    metrics = "Accuracy = " + str(model_LogisticRegression.score(X_test, y_test)) + "\n" + "Confusion matrix for Logistic Regression classifier" + "\n" + str(confusion_matrix(y_test, model_LogisticRegression.predict(X_test)))
    return metrics

def DecisionTree():
    metrics = "Accuracy = " +  str(model_DecisionTree.score(X_test, y_test)) + "\n" + "Confusion matrix for decision tree classifier: " + "\n" + str(confusion_matrix(y_test, model_DecisionTree.predict(X_test)))
    return metrics

def NaiveBayes():
    metrics = "Accuracy = " +  str(model_Bayes.score(X_test, y_test)) + "\n" +  "Confusion matrix for Naive Bayes classifier:" + "\n" + str(confusion_matrix(y_test, model_Bayes.predict(X_test)))
    return metrics

def DrawAccuracyPlot():
    accuracy_list = [model_DecisionTree.score(X_test, y_test), model_Bayes.score(X_test, y_test),
                    model_LogisticRegression.score(X_test, y_test)]
    label = ['Decision Tree', "NaiveBayes", "LogisticRegression"]
    plt.bar(np.arange(len(label)),accuracy_list)
    plt.xlabel("Classifiers")
    plt.ylabel("Accuracy")
    plt.xticks(np.arange(len(label)),label)
    plt.show()


def DrawKfoldResults():
    k_fold_decision_tree_scores = []
    k_fold_bayes_scores = []
    k_fold_logistic_regression_scores = []
    kf = KFold(n_splits=2)
    for train_index, test_index in kf.split(X):
        X_train, X_test = X.iloc[train_index], X.iloc[test_index]
        y_train, y_test = y.iloc[train_index], y.iloc[test_index]
        model_DecisionTree.fit(X_train, y_train)
        k_fold_decision_tree_scores.append(model_DecisionTree.score(X_test, y_test))

        model_Bayes.fit(X_train, y_train)
        k_fold_bayes_scores.append(model_Bayes.score(X_test, y_test))

        model_LogisticRegression.fit(X_train, y_train)
        k_fold_logistic_regression_scores.append(model_LogisticRegression.score(X_test, y_test))


    ax = plt.subplot(111)
    for i in range(len(k_fold_decision_tree_scores)):
      ax.bar(i-0.3, k_fold_decision_tree_scores,width=0.2,color='b',align='center')
    for i in range(len(k_fold_bayes_scores)):
      ax.bar(i, k_fold_bayes_scores,width=0.2,color='g',align='center')
    for i in range(len(k_fold_logistic_regression_scores)):
      ax.bar(i+0.3, k_fold_logistic_regression_scores,width=0.2,color='r',align='center')

    plt.show()


def DrawCrossValidation():
    cross_val_decision_tree = cross_val_score(model_DecisionTree, X, y, cv=5)
    cross_val_naive_bayes = cross_val_score(model_Bayes, X, y, cv=5)
    cross_val_logistic_regression = cross_val_score(model_LogisticRegression, X, y, cv=5)


    ax = plt.subplot(111)
    for i in range(len(cross_val_decision_tree)):
      ax.bar(i-0.3, cross_val_decision_tree,width=0.2,color='b',align='center')
    for i in range(len(cross_val_naive_bayes)):
      ax.bar(i, cross_val_naive_bayes,width=0.2,color='g',align='center')
    for i in range(len(cross_val_logistic_regression)):
      ax.bar(i+0.3, cross_val_logistic_regression,width=0.2,color='r',align='center')

    plt.show()

precision_decision_tree, recall_decision_tree, fscore_decision_tree, support_decision_tree = precision_recall_fscore_support(
    y_test, model_DecisionTree.predict(X_test), average = 'macro')

precision_naive_bayes, recall_naive_bayes, fscore_naive_bayes, support_naive_bayes = precision_recall_fscore_support(
    y_test, model_Bayes.predict(X_test), average = 'macro')

precision_logistic_regression, recall_logistic_regression, fscore_logistic_regression, support_logistic_regression = \
    precision_recall_fscore_support(y_test, model_LogisticRegression.predict(X_test), average = 'macro')


def DecTreeMetrics():
    metrics = "Recall (Decision Tree) - "+ str(recall_decision_tree) + "\n" + \
      "Precision (Decision Tree) - "+ str(precision_decision_tree) + "\n" + \
      "f-score (Decision Tree) - "+ str(fscore_decision_tree) + "\n"
    return metrics

def NaiveBayesMetrics():
    metrics = "Recall (Naive Bayes) - "+ str(recall_naive_bayes) + "\n" + \
              "Precision (Naive Bayes) - "+ str(precision_naive_bayes) + "\n" + \
              "f-score (Naive Bayes) - "+ str(precision_naive_bayes) + "\n"
    return metrics

def LogRegMetrics():
    metrics = "Recall (Logistic Regression) : " + str(recall_logistic_regression) + "\n" + \
              "Precision (Logistic Regression) : " + str(precision_naive_bayes) + "\n" + \
              "f-score (Logistic Regression) : " + str(fscore_logistic_regression) + "\n"
    return metrics

def DrawPrecisionPlot():
    precision_list = [precision_decision_tree,precision_naive_bayes, precision_logistic_regression ]
    label = ['Decision Tree', "Naive Bayes", "Logistic Regression"]
    plt.bar(np.arange(len(label)),precision_list)
    plt.xlabel("Classifiers")
    plt.ylabel("Precision")
    plt.xticks(np.arange(len(label)),label)
    plt.show()

def DrawRecallPlot():
    recall_list = [recall_decision_tree, recall_naive_bayes, recall_logistic_regression ]
    label = ['Decision Tree', "Naive Bayes", "Logistic Regression"]
    plt.bar(np.arange(len(label)),recall_list)
    plt.xlabel("Classifiers")
    plt.ylabel("Recall")
    plt.xticks(np.arange(len(label)),label)
    plt.show()

def DrawFScorePlot():
    fscore_list = [fscore_decision_tree, fscore_naive_bayes, fscore_logistic_regression ]
    label = ['Decision Tree', "Naive Bayes", "Logistic Regression"]
    plt.bar(np.arange(len(label)),fscore_list)
    plt.xlabel("Classifiers")
    plt.ylabel("F-Score")
    plt.xticks(np.arange(len(label)),label)
    plt.show()
