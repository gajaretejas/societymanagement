import csv
from tkinter import *
import random
import time
import preFInalCode as ml

root = Tk()
root.geometry("1366x768+0+0")
root.title("Churn Modelling System")

Header=Frame(root,width =1600,height=50,bg="powder blue",relief="sunken")
Header.pack(side=TOP)

MetricsFrame=Frame(root,width =800,height=500,relief="sunken")
MetricsFrame.pack(side=LEFT)

PlotFrame=Frame(root,width =800,height=500,relief="sunken")
PlotFrame.pack(side=RIGHT)

localtime=time.asctime(time.localtime(time.time()))

headerContent=Label(Header,font=('arial',50,'bold'),text="Churn Modelling System",fg="DodgerBlue4",bd=10,anchor='w')
headerContent.grid(row=0,column=0)

headerContent=Label(Header,font=('arial',20,'bold'),text=localtime,fg="DodgerBlue4",bd=10,anchor='w')
headerContent.grid(row=1,column=0)

#======================Button Functions===========================

def RemoveOutliers():
    displayOutput.delete('1.0', END)
    ml.remove_outlier()
    displayOutput.insert(INSERT, "Outliers Removed From : CreditScore, Age, NumOfProducts")

def ScaleData():
    displayOutput.delete('1.0', END)
    displayOutput.insert(INSERT, "Scaling data of columns: CreditScore, Age, Balance, EstimatedSalary")
    ml.ScaleData()
    displayOutput.insert(INSERT, "Scaling Successfull")

def TreatCatVar():
    displayOutput.delete('1.0', END)
    displayOutput.insert(INSERT, "Treating Categorical Variables of Columns: Gender Geography")
    ml.TreatCategorical()
    displayOutput.insert(INSERT, "Encoding Successfull")

def LogisticRegression():
    displayOutput.delete('1.0', END)
    displayOutput.insert(INSERT, ml.LogisticRegression())
    displayOutput.insert(INSERT, "\n" + ml.LogRegMetrics())


def NaiveBayes():
    displayOutput.delete('1.0', END)
    displayOutput.insert(INSERT, ml.NaiveBayes())
    displayOutput.insert(INSERT, "\n" + ml.NaiveBayesMetrics())

def DecisionTree():
    displayOutput.delete('1.0', END)
    displayOutput.insert(INSERT, ml.DecisionTree())
    displayOutput.insert(INSERT, "\n" + ml.DecTreeMetrics())

def AccuracyPlot():
    displayOutput.delete('1.0', END)
    ml.DrawAccuracyPlot()

def KFoldPlot():
    displayOutput.delete('1.0', END)
    ml.DrawKfoldResults()

def CrossValidationPlot():
    displayOutput.delete('1.0', END)
    ml.DrawCrossValidation()

def RecallPlot():
    displayOutput.delete('1.0', END)
    ml.DrawRecallPlot()

def FScoreplot():
    displayOutput.delete('1.0', END)
    ml.DrawFScorePlot()

def PrecisionPlot():
    displayOutput.delete('1.0', END)
    ml.DrawPrecisionPlot()

#-------------------------BUTTONS-----------------------------------

RemoveOutlierButton=Button(MetricsFrame,padx=16,pady=0,bd=16, fg="black",font=('arial',10,'bold'),width=40,text="Remove Outliers",bg='DeepSkyBlue2',command=RemoveOutliers).grid(row=3,column=7)
ScaleDataButton=Button(MetricsFrame,padx=16,pady=0,bd=16, fg="black",font=('arial',10,'bold'),width=40,text="Scale Data",bg='DeepSkyBlue2',command=ScaleData).grid(row=5,column=7)
TreatDataButton=Button(MetricsFrame,padx=16,pady=0,bd=16, fg="black",font=('arial',10,'bold'),width=40,text="Treat Categorical Variables",bg='DeepSkyBlue2',command=TreatCatVar).grid(row=7,column=7)
AppLogRegButton=Button(MetricsFrame,padx=16,pady=0,bd=16, fg="black",font=('arial',10,'bold'),width=40,text="Apply Logistic Regression Classifier",bg='DeepSkyBlue3',command=LogisticRegression).grid(row=11,column=7)
AppNaivBayButton=Button(MetricsFrame,padx=16,pady=0,bd=16, fg="black",font=('arial',10,'bold'),width=40,text="Apply Naive Bayes Classifier",bg='DeepSkyBlue4',command=NaiveBayes).grid(row=13,column=7)
DecTreeClassButton=Button(MetricsFrame,padx=16,pady=0,bd=16, fg="black",font=('arial',10,'bold'),width=40,bg="DeepSkyBlue4",text='Apply Decision Tree Classifier', command=DecisionTree).grid(row=15,column=7)

AccuracyPlotButton=Button(PlotFrame,padx=16,pady=0,bd=16, fg="black",font=('arial',10,'bold'),width=40,text="Accuracy Plot",bg='DeepSkyBlue2',command=AccuracyPlot).grid(row=3,column=7)
KFoldPlotButton=Button(PlotFrame,padx=16,pady=0,bd=16, fg="black",font=('arial',10,'bold'),width=40,text="KFoldPlot",bg='DeepSkyBlue2',command=KFoldPlot).grid(row=5,column=7)
CrossValidPlotButton=Button(PlotFrame,padx=16,pady=0,bd=16, fg="black",font=('arial',10,'bold'),width=40,text="Cross Validation Plot",bg='DeepSkyBlue2',command=CrossValidationPlot).grid(row=7,column=7)
PrecisionPlotButton=Button(PlotFrame,padx=16,pady=0,bd=16, fg="black",font=('arial',10,'bold'),width=40,text="Precision Plot",bg='DeepSkyBlue3',command=PrecisionPlot).grid(row=11,column=7)
RecallPlotButton=Button(PlotFrame,padx=16,pady=0,bd=16, fg="black",font=('arial',10,'bold'),width=40,text="Recall plot",bg='DeepSkyBlue4',command=RecallPlot).grid(row=13,column=7)
FScorePlotButton=Button(PlotFrame,padx=16,pady=0,bd=16, fg="black",font=('arial',10,'bold'),width=40,bg="DeepSkyBlue4",text='F-Score Plot', command=FScoreplot).grid(row=15,column=7)


TextBoxFrame=Frame(root,width=900,height=1500,relief="sunken")
TextBoxFrame.pack(side=LEFT)
displayOutput=Text(TextBoxFrame,font=('arial',12,'bold'),bd=10, insertwidth=4,bg="DodgerBlue1")
displayOutput.grid(row=19,column=17)
displayOutput.pack()


root.mainloop()
