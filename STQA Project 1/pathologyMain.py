import csv
from tkinter import *
import random
import time

root = Tk()
root.geometry("1366x768+0+0")
root.title("Pathology Lab Management System")


file = open('input.csv', 'a')

############### BILLING FORMAT ####################

my_dict={
    'Reference_Number':'',
    'Patient_Name':'',
    'Test_Name':'',
    'Pathologist_Name':'',
    'Contact_Number':'',
    'Gender':'',
    'Age':'',
    'Test_Date':'',
    'Test_Charges':'',
    'Equipment_Charges':'',
    'Service_Tax':'',
    'Total':''
    }


text_input=StringVar()
operator=""

Tops=Frame(root,width =1600,height=50,bg="powder blue",relief="sunken")
Tops.pack(side=TOP)

f1=Frame(root,width =800,height=500,relief="sunken")
f1.pack(side=LEFT)

f2=Frame(root,width =600,height=300,relief="sunken")
f2.pack(side=RIGHT)

localtime=time.asctime(time.localtime(time.time()))

#================================Time=================

lbl1Info=Label(Tops,font=('arial',50,'bold'),text="Pathology Lab Management System",fg="DodgerBlue4",bd=10,anchor='w')
lbl1Info.grid(row=0,column=0)

lbl1Info=Label(Tops,font=('arial',20,'bold'),text=localtime,fg="DodgerBlue4",bd=10,anchor='w')
lbl1Info.grid(row=1,column=0)
#===========================Calculator======================
def btnClick(numbers):
    global operator
    operator=operator+str(numbers)
    text_input.set(operator)


def btnClearDisplay():
    global operator
    operator=""
    text_input.set("")

def btnEqualsInput():
    global operator
    sumup=str(eval(operator))
    text_input.set(sumup)
    operator=""

def Ref():
    x=random.randint(10643,592365)
    randomRef=str(x)
    rand.set(randomRef)

    #CoF=float()

def qExit():
    my_dict['Reference_Number']=txtReference.get()

    my_dict['Patient_Name']=txtPatient.get()

    my_dict['Test_Name']=txtTreatment.get()

    my_dict['Pathologist_Name']=txtDoctor.get()

    my_dict['Contact_Number']=txtCustomer.get()

    my_dict['Gender']=txtOperation.get()

    my_dict['Age']=txtConsultancy.get()

    my_dict['Test_Date']=txtMedicine.get()

    my_dict['Test_Charges']=txtAccomodation.get()

    my_dict['Equipment_Charges']=txtState.get()

    my_dict['Service_Tax']=txtService.get()

    my_dict['Total']=int(my_dict['Test_Charges'])+int(my_dict['Equipment_Charges'])+int(my_dict['Service_Tax'])
    txtTotal.insert(INSERT, my_dict['Total'])
    

######################################### ALL FUNCTIONS ###############################################

def Reset():
    rand.set("")
    Customer.set("")
    Accomodation.set("")
    Consultancy.set("")
    Operation.set("")
    State.set("")
    Medicine.set("")
    Service.set("")
    Patient.set("")
    Treatment.set("")
    Doctor.set("")

def openSearch():
    def dispSearch():
        username = txtSearch.get()

        with open('input.csv', 'rt') as f:
            reader = csv.reader(f, delimiter=',') # good point by @paco
            for row in reader:
                for field in row:
                    if field == username:
                        displaySearch.insert(INSERT, "\nReference Number: ")
                        displaySearch.insert(INSERT, row[0])
                        displaySearch.insert(INSERT, "\nName: ")
                        displaySearch.insert(INSERT, row[1])
                        displaySearch.insert(INSERT, "\nTreatment: ")
                        displaySearch.insert(INSERT, row[2])
                        displaySearch.insert(INSERT, "\nDoctor's name: ")
                        displaySearch.insert(INSERT, row[3])
                        displaySearch.insert(INSERT, "\nTotal bill: ")
                        displaySearch.insert(INSERT, row[11])
                        displaySearch.insert(INSERT,"\n------------------------")

    root1 = Tk()
    root1.geometry("1200x700+0+0")
    root1.title("Search HERE")

    text_input=StringVar()
    operator=""

    Tops=Frame(root1,width=1200,height=50,bg="powder blue",relief="sunken")
    Tops.pack(side=TOP)

    frame=Frame(root1,width=400,height=500,relief="sunken")
    frame.pack(side=LEFT)

    frame1=Frame(root1,width=400,height=500,relief="sunken")
    frame1.pack(side=RIGHT)


    lblSearch=Label(frame,font=('arial',12,'bold'),text="Enter Patient's name:  ",bd=16, anchor='w')
    lblSearch.grid(row=0,column=0)

    a = ''
    txtSearch=Entry(frame,font=('arial',12,'bold'),textvariable=a,bd=10, insertwidth=4,bg="DodgerBlue2",justify='right')
    txtSearch.grid(row=0,column=1)

    lblSearch2=Label(frame,font=('arial',12,'bold'),text=" OR ",bd=16, anchor='w')
    lblSearch2.grid(row=2,column=0)


    lblSearch1=Label(frame,font=('arial',12,'bold'),text="Enter Customer membership number:  ",bd=16, anchor='w')
    lblSearch1.grid(row=4,column=0)

    a = ''
    txtSearch1=Entry(frame,font=('arial',12,'bold'),textvariable=a,bd=10, insertwidth=4,bg="DodgerBlue2",justify='right')
    txtSearch1.grid(row=4,column=1)



    displaySearch=Text(frame1,font=('arial',12,'bold'),bd=10, insertwidth=4,bg="DodgerBlue1")
    displaySearch.grid(row=0,column=1)

    displaySearch.pack()

    Search1=Button(frame,padx=16,pady=0,bd=16, fg="black",font=('arial',16,'bold'),width=10,text="Search",bg='DeepSkyBlue3', command=dispSearch).grid(row=6,column=1)
    root1.mainloop()


def getInput():

    my_dict['Reference_Number']=txtReference.get()
    txtReference.delete(0, END)

    my_dict['Patient_Name']=txtPatient.get()
    txtPatient.delete(0, END)

    my_dict['Test_Name']=txtTreatment.get()
    txtTreatment.delete(0, END)

    my_dict['Pathologist_Name']=txtDoctor.get()
    txtDoctor.delete(0, END)

    my_dict['Contact_Number']=txtCustomer.get()
    txtCustomer.delete(0, END)

    my_dict['Gender']=txtOperation.get()
    txtOperation.delete(0, END)

    my_dict['Age']=txtConsultancy.get()
    txtConsultancy.delete(0, END)

    my_dict['Test_Date']=txtMedicine.get()
    txtMedicine.delete(0, END)

    my_dict['Test_Charges']=txtAccomodation.get()
    txtAccomodation.delete(0, END)

    my_dict['Equipment_Charges']=txtState.get()
    txtState.delete(0, END)

    my_dict['Service_Tax']=txtService.get()
    txtService.delete(0, END)

    my_list=[]
    
    if my_dict['Patient_Name'].isdigit() or my_dict['Patient_Name']=='':
        my_list.append('Patient_Name')

    if my_dict['Pathologist_Name'].isdigit() or my_dict['Pathologist_Name']=='':
        my_list.append('Pathologist_Name')

    if my_dict['Contact_Number'].isdigit() != 1 or my_dict['Contact_Number']=='' or len(my_dict['Contact_Number'])!=10:
        my_list.append('Contact_Number')

    if my_dict['Test_Charges'].isdigit() != 1 or my_dict['Test_Charges']=='':
        my_list.append('Test_Charges')

    if my_dict['Equipment_Charges'].isdigit()!=1 or my_dict['Equipment_Charges']=='':
        my_list.append('Equipment_Charges')

    if my_dict['Service_Tax'].isdigit()!=1 or my_dict['Service_Tax']=='':
        my_list.append('Service_Tax')

    if my_dict['Age'].isdigit()!=1 or len(my_dict['Age'])>2 or my_dict['Age']=='':
        my_list.append('Accomodation Charges')


    if len(my_list)==0:
        printBIll()
        print(my_dict)


    if len(my_list)>0:
        root1 = Tk()
        root1.geometry("700x300+0+0")
        root1.title("Error!")

        frame=Frame(root1,width=400,height=500,relief="sunken")
        frame.pack(side=LEFT)

        frame1=Frame(root1,width=400,height=500,relief="sunken")
        frame1.pack(side=RIGHT)
        my_text="Recheck your entries. Incorrect values of the following have been entered:  "
        my_text1=''
    for k in my_list:
        my_text1=my_text1+" "+k+" , "
        lblSearch1=Label(frame,font=('arial',12,'bold'),text=my_text,bd=16, anchor='w')
        lblSearch1.grid(row=0,column=0)
        lblSearch2=Label(frame,font=('arial',12,'bold'),text=my_text1,bd=16, anchor='w')
        lblSearch2.grid(row=1,column=0)

    root1.mainloop()


def printBIll():
    with open('input.csv', 'a') as csvfile:
        fieldnames = ['Reference_Number','Patient_Name','Test_Name','Pathologist_Name','Contact_Number','Gender','Age','Test_Date','Test_Charges','Equipment_Charges','Service_Tax','Total']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

        #writer.writeheader()
        writer.writerow(my_dict)




def plotGraph():
    from csv import reader
    import numpy as np
    import matplotlib.pyplot as plt

    with open('input.csv', 'r') as f:
        read = list(reader(f))


    labels = ['Dengue','Diabetes','AIDS','Nipha','Pregnancy','Cancer']
    disFreq = []

    diseases = {
            'Dengue':0,
            'Diabetes':0,
            'AIDS':0,
            'Nipha':0,
            'Pregnancy':0,
            'Cancer':0
        }


    for i in read:
        if i[2] in diseases:
            diseases[i[2]] += 1
        else:
            continue

    print(diseases)

    for i in labels:
        disFreq.append(diseases[i])

     # only "explode" the 2nd slice (i.e. 'Hogs')
    print(disFreq)

    fig1, ax1 = plt.subplots()
    ax1.pie(disFreq, labels=labels, autopct='%1.1f%%',shadow=True, startangle=90)
    ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.

    plt.show()


#-------------------BILLING INFO---------------------------

rand=StringVar()
Customer=StringVar()
Accomodation=StringVar()
Consultancy=StringVar()
Operation=StringVar()
State=StringVar()
Medicine=StringVar()
Service=StringVar()
Patient=StringVar()
Treatment=StringVar()
Doctor=StringVar()
Total=StringVar()


lblReference=Label(f1,font=('arial',12,'bold'),text="Reference Number: ",bd=16, anchor='w')
lblReference.grid(row=0,column=0)
txtReference=Entry(f1,font=('arial',12,'bold'),textvariable=rand,bd=10, insertwidth=4,bg="DodgerBlue2",justify='right')
txtReference.grid(row=0,column=1)


lblPatient=Label(f1,font=('arial',12,'bold'),text="Patient Name: ",bd=16, anchor='w')
lblPatient.grid(row=1,column=0)
txtPatient=Entry(f1,font=('arial',12,'bold'),textvariable=Patient,bd=10, insertwidth=4,bg="DodgerBlue3",justify='right')
txtPatient.grid(row=1,column=1)

lblTreatment=Label(f1,font=('arial',12,'bold'),text="Test Name: ",bd=16, anchor='w')
lblTreatment.grid(row=2,column=0)
txtTreatment=Entry(f1,font=('arial',12,'bold'),textvariable=Treatment,bd=10, insertwidth=4,bg="DodgerBlue4",justify='right')
txtTreatment.grid(row=2,column=1)


lblDoctor=Label(f1,font=('arial',12,'bold'),text="Pathologist Name:",bd=16, anchor='w')
lblDoctor.grid(row=3,column=0)
txtDoctor=Entry(f1,font=('arial',12,'bold'),textvariable=Doctor,bd=10, insertwidth=4,bg="DodgerBlue4",justify='right')
txtDoctor.grid(row=3,column=1)


lblCustomer=Label(f1,font=('arial',12,'bold'),text="Contact Number: ",bd=16, anchor='w')
lblCustomer.grid(row=4,column=0)
txtCustomer=Entry(f1,font=('arial',12,'bold'),textvariable=Customer,bd=10, insertwidth=4,bg="DodgerBlue3",justify='right')
txtCustomer.grid(row=4,column=1)


lblOperation=Label(f1,font=('arial',12,'bold'),text="Sex: ",bd=16, anchor='w')
lblOperation.grid(row=5,column=0)
txtOperation=Entry(f1,font=('arial',12,'bold'),textvariable=Operation,bd=10, insertwidth=4,bg="DodgerBlue2",justify='right')
txtOperation.grid(row=5,column=1)


#---------------------------NEXT COLUMN--------------------------

lblCollection=Label(f1,font=('arial',14,'bold'),text="Age: ",bd=16, anchor='w')
lblCollection.grid(row=0,column=4)
txtConsultancy=Entry(f1,font=('arial',14,'bold'),textvariable=Consultancy,bd=10, insertwidth=4,bg="DodgerBlue4",justify='right')
txtConsultancy.grid(row=0,column=5)

lblMedicine=Label(f1,font=('arial',14,'bold'),text="Test Date: ",bd=16, anchor='w')
lblMedicine.grid(row=1,column=4)
txtMedicine=Entry(f1,font=('arial',14,'bold'),textvariable=Medicine,bd=10, insertwidth=4,bg="DodgerBlue3",justify='right')
txtMedicine.grid(row=1,column=5)

lblAccomodation=Label(f1,font=('arial',14,'bold'),text="Test charges: ",bd=16, anchor='w')
lblAccomodation.grid(row=2,column=4)
txtAccomodation=Entry(f1,font=('arial',14,'bold'),textvariable=Accomodation,bd=10, insertwidth=4,bg="DodgerBlue2",justify='right')
txtAccomodation.grid(row=2,column=5)

lblState=Label(f1,font=('arial',14,'bold'),text="Equipment charges: ",bd=16, anchor='w')
lblState.grid(row=3,column=4)
txtState=Entry(f1,font=('arial',14,'bold'),textvariable=State,bd=10, insertwidth=4,bg="DodgerBlue2",justify='right')
txtState.grid(row=3,column=5)

lblService=Label(f1,font=('arial',14,'bold'),text="Service tax: ",bd=16, anchor='w')
lblService.grid(row=4,column=4)
txtService=Entry(f1,font=('arial',14,'bold'),textvariable=Service,bd=10, insertwidth=4,bg="DodgerBlue3",justify='right')
txtService.grid(row=4,column=5)

lblTotal=Label(f1,font=('arial',14,'bold'),text="Grand total: ",bd=16, anchor='w')
lblTotal.grid(row=5,column=4)
txtTotal=Entry(f1,font=('arial',14,'bold'),textvariable=Total,bd=10, insertwidth=4,bg="DodgerBlue4",justify='right')
txtTotal.grid(row=5,column=5)

#-------------------------BUTTONS-----------------------------------

btnTotal=Button(f1,padx=16,pady=0,bd=16, fg="black",font=('arial',16,'bold'),width=10,text="GenRef",bg='DeepSkyBlue2',command=Ref).grid(row=8,column=2)

btnReset=Button(f1,padx=16,pady=0,bd=16, fg="black",font=('arial',16,'bold'),width=10,text="Reset",bg='DeepSkyBlue3',command=Reset).grid(row=7,column=1)

btnExit=Button(f1,padx=16,pady=0,bd=16, fg="black",font=('arial',16,'bold'),width=10,text="Total",bg='DeepSkyBlue4',command=qExit).grid(row=7,column=2)

Input=Button(f1,padx=16,pady=0,bd=16, fg="black",font=('arial',16,'bold'),width=10,text="Input",bg='DeepSkyBlue4', command=getInput).grid(row=7,column=3)

Search=Button(f1,padx=16,pady=0,bd=16, fg="black",font=('arial',16,'bold'),width=10,text="Search",bg='DeepSkyBlue3', command=openSearch).grid(row=7,column=4)

GenStats=Button(f1,padx=16,pady=0,bd=16, fg="black",font=('arial',16,'bold'),width=10,text="GenerateStats",bg='DeepSkyBlue2', command=plotGraph).grid(row=8,column=3)


root.mainloop()
