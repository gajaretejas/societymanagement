from django.contrib import admin

from .models import Visitor, Announcement, Member, Task

admin.site.register(Visitor)
admin.site.register(Announcement)
admin.site.register(Member)
admin.site.register(Task)
