from django.db import models
from django.urls import reverse

class Visitor(models.Model):
    visitorID = models.AutoField(primary_key=True)
    visitorNAME = models.CharField(max_length = 50)
    visitorCONTACT = models.IntegerField()
    visitorAPART = models.CharField(max_length = 4)
    visitorVEHICLE = models.CharField(max_length = 10)
    visitorEntryDATE = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.visitorNAME

    def get_absolute_url(self):
        return reverse('addVisitor')

class Member(models.Model):
    memberID = models.AutoField(primary_key = True)
    memberNAME = models.CharField(max_length = 50)
    memberAPART = models.CharField(max_length = 4)
    memberCONTACT = models.IntegerField()

    def __str__(self):
        return self.memberNAME

class Task(models.Model):
    taskID = models.AutoField(primary_key = True)
    taskDESC = models.CharField(max_length = 50)

    def __str__(self):
        return self.taskDESC[:10]


class Announcement(models.Model):
    announcementID = models.AutoField(primary_key = True)
    announcementDESC = models.CharField(max_length = 50)
    announcementDATE = models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return self.announcementDESC[:10]
