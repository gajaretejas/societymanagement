# Generated by Django 2.1.2 on 2018-10-24 04:46

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0003_announcements_members_tasks'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Announcements',
            new_name='Announcement',
        ),
        migrations.RenameModel(
            old_name='Members',
            new_name='Member',
        ),
        migrations.RenameModel(
            old_name='Tasks',
            new_name='Task',
        ),
    ]
