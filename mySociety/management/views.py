from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.views.generic.edit import CreateView
from .models import Visitor, Member, Announcement, Task



def user_login(request):
    context = {}
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']

        user = authenticate(request, username=username, password=password)
        if user:
            login(request, user)
            return HttpResponseRedirect(reverse('chairman'))
        else:
            context["error"] = "Please provide valid credentials !!"
            return render(request, "registration/login.html", context)
    else:
        return render(request, "registration/login.html", context)

def index(request):
    context = {
        "visitor_count": Visitor.objects.all().count(),
        "member_count": Member.objects.all().count(),
        "task_count": Task.objects.all().count(),
    }
    context['user'] = request.user
    return render(request, "management/home.html", context)

@login_required(login_url='/login/')
def chairman(request):
    context = {
        "visitor_count": Visitor.objects.all().count(),
        "member_count": Member.objects.all().count(),
        "task_count": Task.objects.all().count(),
    }
    context['user'] = request.user
    context['announcements'] = Announcement.objects.all()
    
    if request.method == 'POST':
        add_type = request.POST.get('add_type')
        print('Add Type is', add_type)
        if add_type == 'announcement':
            announcement = Announcement()
            announcement.announcementDESC = request.POST.get('announcementDESC')
            announcement.save()
        elif add_type == 'member':
            member = Member()
            member.memberNAME = request.POST.get('memberNAME')
            member.memberAPART = request.POST.get('memberAPART')
            member.memberCONTACT = request.POST.get('memberCONTACT')
            member.save()
        elif add_type == 'task':
            task = Task()
            task.taskDESC = request.POST.get('taskDESC')
            task.save()

    return render(request, "management/chairman.html", context)

def user_logout(request):
    logout(request)
    return HttpResponseRedirect(reverse('user_login'))
    
@login_required(login_url='/login/')
def maintainanceList(request):
    context = {
        "visitor_count": Visitor.objects.all().count(),
        "member_count": Member.objects.all().count(),
        "task_count": Task.objects.all().count(),
    }
    context['tasks'] = Task.objects.all()
    if request.method == 'POST':
        add_type = request.POST.get('add_type')
        print('Add Type is', add_type)
        if add_type == 'announcement':
            announcement = Announcement()
            announcement.announcementDESC = request.POST.get('announcementDESC')
            announcement.save()
        elif add_type == 'member':
            member = Member()
            member.memberNAME = request.POST.get('memberNAME')
            member.memberAPART = request.POST.get('memberAPART')
            member.memberCONTACT = request.POST.get('memberCONTACT')
            member.save()
        elif add_type == 'task':
            task = Task()
            task.taskDESC = request.POST.get('taskDESC')
            task.save()

    # if request.method == 'POST':
    #     task = Task()
    #     task.taskDESC = request.POST.get('taskDESC')
    #     task.save()

    return render(request, "management/maintainanceList.html", context)

@login_required(login_url='/login/')
def memberList(request):
    context = {
        "visitor_count": Visitor.objects.all().count(),
        "member_count": Member.objects.all().count(),
        "task_count": Task.objects.all().count(),
    }
    context['members'] = Member.objects.all()
    if request.method == 'POST':
        add_type = request.POST.get('add_type')
        print('Add Type is', add_type)
        if add_type == 'announcement':
            announcement = Announcement()
            announcement.announcementDESC = request.POST.get('announcementDESC')
            announcement.save()
        elif add_type == 'member':
            member = Member()
            member.memberNAME = request.POST.get('memberNAME')
            member.memberAPART = request.POST.get('memberAPART')
            member.memberCONTACT = request.POST.get('memberCONTACT')
            member.save()
        elif add_type == 'task':
            task = Task()
            task.taskDESC = request.POST.get('taskDESC')
            task.save()

    # if request.method == 'POST':
    #     member = Member()
    #     member.memberNAME = request.POST.get('memberNAME')
    #     member.memberAPART = request.POST.get('memberAPART')
    #     member.memberCONTACT = request.POST.get('memberCONTACT')
    #     member.save()
    #     print(Member.objects.all)
        
    return render(request, "management/memberList.html", context)

@login_required(login_url='/login/')
def visitorList(request):
    context = {
        "visitor_count": Visitor.objects.all().count(),
        "member_count": Member.objects.all().count(),
        "task_count": Task.objects.all().count(),
    }
    context['visitors'] = Visitor.objects.all()
    return render(request, "management/visitorList.html", context)

def watchman(CreateView):
    if request.method == 'POST':
        form = EntryForm(request.POST)
        context['form'] = form
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('watchman'))
        else:
            return render(request, "management/entry.html", context)    
    else:
        form = EntryForm()
        context['form'] = form   
        return render(request, "management/entry.html", context)

class addVisitor(CreateView):
    model = Visitor
    fields = [
            'visitorNAME',
            'visitorCONTACT',
            'visitorAPART',
            'visitorVEHICLE',
        ]