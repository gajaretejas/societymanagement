from django.urls import path
from . import views

urlpatterns = [
    path('', views.chairman, name='chairman'),
    path('chairman/', views.chairman, name="chairman"),

    path('tasks/', views.maintainanceList, name="maintainanceList"),
    path('members/', views.memberList, name="memberList"),
    path('visitors/', views.visitorList, name="visitorList"),

    path('watchman/', views.addVisitor.as_view(), name="addVisitor"),
]
