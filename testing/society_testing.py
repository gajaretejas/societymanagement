from selenium import webdriver
import unittest
from selenium.webdriver.common.keys import Keys

class SocietyManagement(unittest.TestCase):

    def setUp(self):
        self.browser = webdriver.Chrome('chromedriver.exe')
        self.base_url = 'http://127.0.0.1:8000/'

        self.browser.get(self.base_url + 'login')
        username = self.browser.find_element_by_id("inputEmail")
        username.clear()
        username.send_keys("admin")

        password = self.browser.find_element_by_id("inputPassword")
        password.clear()
        password.send_keys("admin123")

        submit = self.browser.find_element_by_id("submit")
        password.send_keys(Keys.RETURN)

    def tearDown(self):
        self.browser.quit()

    def test_loginpage_title_is_correct(self):
        self.browser.get(self.base_url + 'login')
        self.assertIn('Login', self.browser.title)

    def test_chairman_title_is_correct(self):
        self.browser.get(self.base_url + 'chairman')
        self.assertIn('Admin | Dashboard', self.browser.title)

    def test_chairman_has_navbar(self):
        self.browser.get(self.base_url + 'chairman')
        self.assertTrue(self.browser.find_element_by_class_name('navbar'))

    def test_tasks_has_navbar(self):
        self.browser.get(self.base_url + 'tasks')
        self.assertTrue(self.browser.find_element_by_class_name('navbar'))

    def test_watchman_has_form(self):
        self.browser.get(self.base_url + 'watchman')
        self.assertTrue(self.browser.find_element_by_tag_name('form'))

    def test_member_link_is_correct(self):
        self.browser.get(self.base_url + 'members')
        self.browser.find_element_by_id('memberList').click()
        self.assertEqual(self.browser.current_url, self.base_url + 'members/')

if __name__ == '__main__':
    unittest.main(warnings='ignore', verbosity=2)
